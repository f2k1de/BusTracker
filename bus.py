import urllib.request, json, os, numpy as np, copy
from typing import Tuple, Dict, List
from dataclasses import dataclass

@dataclass
class Delta: # 14 Bytes
    id: np.uint16
    we: np.int16 # Delta
    ns: np.int16 # Delta
    dir: np.uint16
    speed: np.uint16
    timestamp: np.uint32

    def __init__(self, id : np.uint16, we : np.int16, ns : np.int16, dir : np.uint16, speed : np.uint16, timestamp : np.uint32):
        self.id = np.uint16(id)
        self.we = np.int16(we)
        self.ns = np.int16(ns)
        self.dir = np.uint16(dir)
        self.speed = np.uint16(speed)
        self.timestamp = np.uint32(timestamp)

    def serialize(self) -> bytes:
        return self.id.tobytes() + self.we.tobytes() + self.ns.tobytes() + self.dir.tobytes() + self.speed.tobytes() + self.timestamp.tobytes()
    
    @staticmethod
    def deserialize(data : bytes):
        a = np.frombuffer(data, dtype=np.uint16, count=1, offset=0)
        b = np.frombuffer(data, dtype=np.int16 , count=2, offset=2)
        c = np.frombuffer(data, dtype=np.uint16, count=2, offset=6)
        d = np.frombuffer(data, dtype=np.uint32, count=1, offset=10)

        return Delta(a[0], b[0], b[1], c[0], c[1], d[0])

@dataclass
class Bus: # 32 Bytes
    id: np.uint16
    we: np.double
    ns: np.double
    dir: np.uint16
    speed: np.double
    timestamp: np.uint32

    def __init__(self, id : np.uint16, we : np.double, ns : np.double, dir : np.uint16, speed : np.double, timestamp : np.uint32):
        self.id = np.uint16(id)
        self.we = np.double(we)
        self.ns = np.double(ns)
        self.dir = np.uint16(dir)
        self.speed = np.double(speed)
        self.timestamp = np.uint32(timestamp)

    def __add__(self, delta : Delta):
        return Bus(
            self.id,
            self.we + np.double(delta.we) * 1E-6,
            self.ns + np.double(delta.ns) * 1E-6,
            delta.dir,
            np.double(delta.speed) * 1E-2,
            delta.timestamp
        )

    def serialize(self) -> bytes:
        return self.id.tobytes() + self.we.tobytes() + self.ns.tobytes() + self.dir.tobytes() + self.speed.tobytes() + self.timestamp.tobytes()
    
    @staticmethod
    def deserialize(data : bytes):
        return Bus(
            np.frombuffer(data, dtype=np.uint16, count=1, offset=0 )[0],
            np.frombuffer(data, dtype=np.double, count=1, offset=2 )[0],
            np.frombuffer(data, dtype=np.double, count=1, offset=10)[0],
            np.frombuffer(data, dtype=np.uint16, count=1, offset=18)[0],
            np.frombuffer(data, dtype=np.double, count=1, offset=20)[0],
            np.frombuffer(data, dtype=np.uint32, count=1, offset=28)[0],
            )

def getBusses(url):
    response = urllib.request.urlopen(url)
    busses = {}
    for bus in json.loads(response.read()):
        busses[int(bus["id"][-3:])] = Bus(id = int(bus["id"][-3:]), we = bus["WE"], ns = bus["NS"], dir = bus["dir"], speed = float(bus["speed"]), timestamp = bus["unix_ts"])
    return busses

def calculateDeltasFromBusses(busList, newBusses) -> List[Delta]:
    deltas = []
    for id, bus in newBusses.items():
        old = busList[id]
        if bus.we != old.we or bus.ns != old.ns or bus.dir != old.dir or bus.speed != old.speed or bus.timestamp != old.timestamp: # Änderungen prüfen
            deltas.append(Delta(id,
                (bus.we - old.we) * 1E6, (bus.ns - old.ns) * 1E6, # 6 Nachkommastellen
                bus.dir, # Drehung scheint ein Winkel zu sein, da zwischen 0 und 360 macht ein Delta kein Sinn.
                bus.speed * 1E2, # Geschwindigkeit auch wieder nur 3 Ziffern, es werden 2 Nachkommastellen genommen, bei 100 km/h wird es 10000, bis 65535 wird kein Extra Speicher nötig.
                bus.timestamp
            ))
            busList[id] = newBusses[id]
    return deltas

def calculateBussesFromDeltas(initialBusList, deltas):
    busList = copy.deepcopy(initialBusList)
    for delta in deltas:
        busList[delta.id] += delta
    return busList


# Data format:
# Filename is Date in ISO8601
# 4 bytes Version Number
# 4 Bus table size
# (32 bytes Bus table) * n
# (14 bytes deltas) * n

def loadFile(path) -> Tuple[Dict[np.uint16, Bus], List[Delta]]:
    size = np.uint32(os.path.getsize(path))
    with open(path, "rb") as file:
        header = file.read(8)

        version : np.uint32 = np.frombuffer(header, dtype=np.uint32, count=1, offset=0)[0]

        assert version == 1

        busCount : np.uint32 = np.frombuffer(header, dtype=np.uint32, count=1, offset=4)[0]

        busses = {}
        for _ in range(busCount):
            bus = Bus.deserialize(file.read(32))
            busses[bus.id] = bus
            
        deltas = []
        for _ in range(np.uint32((size - 32 * busCount - 8) / 14)):
            delta = Delta.deserialize(file.read(14))
            deltas.append(delta)
        
        return busses, deltas

def loadFileAndCalculateBusses(path) -> Dict[np.uint16, Bus]:
    busses, deltas = loadFile(path)
    return calculateBussesFromDeltas(busses, deltas)

def saveFile(path, busses):
    with open(path, "wb") as file:
        file.write(np.uint32(1).tobytes())
        file.write(np.uint32(len(busses)).tobytes())
        [file.write(bus.serialize()) for id, bus in busses.items()]