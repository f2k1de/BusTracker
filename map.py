import folium, bus, os, random, datetime

datapath = "data"
mapspath = "maps"

if not os.path.exists(mapspath):
    os.makedirs(mapspath)

def generateMap(file):
    busses, deltas = bus.loadFile(os.path.join(datapath, file))

    points = {}
    for id, abus in busses.items():
        points[id] = [[abus.ns, abus.we]]
    
    dc = 0
    for delta in deltas:
        busses[delta.id] += delta
        points[delta.id].append([busses[delta.id].ns, busses[delta.id].we])
        dc = dc + 1
    
    print(dc)

    m = folium.Map(location=[48.85, 2.35], tiles="OpenStreetMap", zoom_start=2)
    #folium.vector_layers.PolyLine([[45, 1],[44,43],[44,45]]).add_to(m)
    for id, line in points.items():
        folium.vector_layers.PolyLine(line, tooptip=str(id), color='#' + str(hex(random.Random(id).randint(0,16777215))).split("x")[1], weight=2).add_to(m)
    m.save(os.path.join(mapspath, file[:-4]) + '.html')

for file in os.listdir(datapath):
    if file.endswith(".bus") and (not os.path.exists(os.path.join(mapspath, file[:-4]) + '.html') or file[:-4] == datetime.datetime.now().isoformat().split('T')[0]):
        generateMap(file)