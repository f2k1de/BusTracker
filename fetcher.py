import bus, os, datetime

datapath = "data"
url = "https://stadtwerke.24stundenonline.de/request.php?action=getpositions"

def main():
    if not os.path.exists(datapath):
        os.makedirs(datapath)
    
    deltaCount = 0

    while True:
        lastOpen = datetime.datetime.now().isoformat().split('T')[0]
        filename = os.path.join(datapath, lastOpen + ".bus")
        print("Writing " + filename)

        busses = {}
        if os.path.exists(filename):
            print("Loading file...")
            busses = bus.loadFileAndCalculateBusses(filename)
        else:
            print("Fetching..")
            busses = bus.getBusses(url)
            bus.saveFile(filename, busses)
        
        with open(filename, "ab") as file:
            while lastOpen == datetime.datetime.now().isoformat().split('T')[0]:
                try:
                    for delta in bus.calculateDeltasFromBusses(busses, bus.getBusses(url)):
                        file.write(delta.serialize())
                        deltaCount += 1
                except TypeError as e:
                    print(e)
                print(deltaCount)

main()