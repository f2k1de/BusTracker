# BusTracker
Bustracker für Schweinfurt ausgelegt für Datensparsamkeit.
## Installation
Benötigt sind Python3, numpy und folium
## Nutzung
Zum Tracken
```bash
    python fetcher.py
```
ausführen.
Und um Karten zu generieren und aktualisieren
```bash
    python map.py
```